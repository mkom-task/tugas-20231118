# Input data member
kode_member = input("Kode member                : ")
nama_member = input("Nama member                : ")
biaya_layanan = float(input("Biaya layanan      : "))
lama_member = int(input("Lama (tahun)           : "))

# Menghitung diskon berdasarkan lamanya menjadi member
diskon = 0

if lama_member >= 20:
    diskon = 0.15 * biaya_layanan
elif lama_member >= 10:
    diskon = 0.10 * biaya_layanan
elif lama_member >= 5:
    diskon = 0.05 * biaya_layanan

# Menghitung total biaya setelah diskon
total_biaya = biaya_layanan - diskon

# Output hasil perhitungan
print("\n=========================")
print("Kode Member          :", kode_member)
print("Nama Member          :", nama_member)
print("Biaya Layanan        :", biaya_layanan)
print("Lama (Tahun)         :", lama_member)
print("Diskon yang didapat  :", diskon)
print("Total Setelah Diskon :", total_biaya)
