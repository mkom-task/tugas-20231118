print('+============================================+')
print('| NIM    : 2211601725                        |')
print('| Nama   : Wahiddin Ishak                    |')
print('| Program untuk mencetak nilai tengah        |')
print('| dari 3 buah nilai yang diinput             |')
print('+============================================+')

# Memasukkan tiga bilangan bulat yang berbeda
num1 = int(input("Angka 1: "))
num2 = int(input("angka 2: "))
num3 = int(input("Angka 3: "))

# Mencari nilai tengah di antara ketiga bilangan
if num1 < num2 < num3 or num3 < num2 < num1:
    middle_number = num2
elif num2 < num1 < num3 or num3 < num1 < num2:
    middle_number = num1
else:
    middle_number = num3

# Mencetak bilangan yang merupakan nilai tengah
print("Nilai Tengah:", middle_number)
