# Fungsi untuk mencetak baris tabel
def print_row(row):
    row_str = "|".join(str(item).ljust(12) for item in row)
    print(f"| {row_str} |")

# Fungsi untuk mencetak garis horizontal tabel
def print_separator():
    separator = "+".join(["=" * 12] * len(header))
    print(f"+{separator}+")

# Inisialisasi variabel
data_mahasiswa = []
total_nilai = 0
nilai_tertinggi = float('-inf')
nilai_terendah = float('inf')
nim_tertinggi = ''
nama_tertinggi = ''
nim_terendah = ''
nama_terendah = ''

# Header laporan
header = ["NIM","Nama","TGS","UTS","UAS","Nilai Akhir","Grade"]

# Input jumlah data mahasiswa
jumlah_data = int(input("Masukkan jumlah data mahasiswa: "))

# Input data untuk setiap mahasiswa
for i in range(jumlah_data):
    print(f"\nMasukkan data untuk Mahasiswa ke-{i+1}:")
    nim = input("NIM: ")
    nama = input("Nama: ")
    nilai_tugas = float(input("Nilai Tugas: "))
    nilai_uts = float(input("Nilai UTS: "))
    nilai_uas = float(input("Nilai UAS: "))
    
    # Hitung nilai akhir
    nilai_akhir = 0.3 * nilai_tugas + 0.3 * nilai_uts + 0.4 * nilai_uas
    
    # Konversi nilai akhir ke dalam grade
    if nilai_akhir >= 90:
        grade = 'A'
    elif nilai_akhir >= 85:
        grade = 'A-'
    elif nilai_akhir >= 80:
        grade = 'B+'
    elif nilai_akhir >= 75:
        grade = 'B'
    elif nilai_akhir >= 70:
        grade = 'B-'
    elif nilai_akhir >= 65:
        grade = 'C+'
    elif nilai_akhir >= 60:
        grade = 'C-'
    elif nilai_akhir >= 50:
        grade = 'D'
    elif nilai_akhir >= 40:
        grade = 'E'
    else:
        grade = 'T'

    # Simpan data mahasiswa ke dalam list
    data_mahasiswa.append({'NIM': nim, 'Nama': nama, 'TGS': nilai_tugas,'UTS':nilai_uts, 'UAS': nilai_uas, 'Nilai Akhir': round(nilai_akhir,2), 'Grade': grade})
    
    # Perhitungan total nilai
    total_nilai += nilai_akhir
    
    # Mencari nilai tertinggi dan terendah beserta NIM dan Nama
    if nilai_akhir > nilai_tertinggi:
        nilai_tertinggi = nilai_akhir
        nim_tertinggi = nim
        nama_tertinggi = nama
    if nilai_akhir < nilai_terendah:
        nilai_terendah = nilai_akhir
        nim_terendah = nim
        nama_terendah = nama

# Hitung rata-rata nilai
rata_rata = total_nilai / jumlah_data

# Output hasil perhitungan
print("\nLaporan Nilai:")
print_separator()
print_row(header)
print_separator()

# for data in data_mahasiswa:
#     print_row(data)
#     print_separator()

for data in data_mahasiswa:
    row = [data[key] for key in header]
    print_row(row)
    print_separator()

print("\nTotal Nilai Semua Mahasiswa:", round(total_nilai,2))
print("Rata-rata Nilai Mahasiswa:", round(rata_rata,2))
print("Nilai Tertinggi:")
print("NIM:", nim_tertinggi)
print("Nama:", nama_tertinggi)
print("Nilai Akhir:", round(nilai_tertinggi,2))
print("Nilai Terendah:")
print("NIM:", nim_terendah)
print("Nama:", nama_terendah)
print("Nilai Akhir:", round(nilai_terendah,2))
